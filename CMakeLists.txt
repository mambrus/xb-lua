cmake_minimum_required (VERSION 3.15.5)
project (xb-lua
    VERSION 5.3.3
    HOMEPAGE_URL "https://gitlab.com/mambrus/xb-lua"
    LANGUAGES C)

set_property(CACHE CMAKE_BUILD_TYPE
    PROPERTY STRINGS
    "None" "Debug" "Release" "RelWithDebInfo" "MinSizeRel")

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
include(GitLabling) # Sets PROJECT_VERSION_TWEAK

include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckCSourceCompiles)
include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckSymbolExists)
include(GNUInstallDirs)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")
include_directories("${CMAKE_CURRENT_BINARY_DIR}/include")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include-lua")
include_directories("${CMAKE_CURRENT_BINARY_DIR}/include-lua")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/lua")
include_directories("${CMAKE_CURRENT_BINARY_DIR}/lua")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

CHECK_LIBRARY_EXISTS(rt clock_gettime "" HAVE_LIB_RT)
CHECK_LIBRARY_EXISTS(pthread pthread_create "" HAVE_LIB_PTHREAD)
CHECK_LIBRARY_EXISTS(m sin "" HAVE_LIB_M)

if (HAVE_LIB_RT)
    set(EXTRA_LIBS ${EXTRA_LIBS} rt)
endif ()
if (HAVE_LIB_PTHREAD)
    set(EXTRA_LIBS ${EXTRA_LIBS} pthread)
endif ()
if (HAVE_LIB_M)
    set(EXTRA_LIBS ${EXTRA_LIBS} m)
endif ()

CHECK_LIBRARY_EXISTS(dl dlopen "" HAVE_LIB_DL)
CHECK_LIBRARY_EXISTS(readline rl_instream "" HAVE_LIB_RL)
CHECK_LIBRARY_EXISTS(history history_search "" HAVE_LIB_HISTORY)
CHECK_LIBRARY_EXISTS(ncurses getcurx "" HAVE_LIB_NCURSES)

if (HAVE_LIB_DL)
    set(GOODI_LIBS ${GOODI_LIBS} dl)
endif ()
if (HAVE_LIB_RL)
    set(GOODI_LIBS ${GOODI_LIBS} readline)
endif ()
if (HAVE_LIB_HISTORY)
    set(GOODI_LIBS ${GOODI_LIBS} history)
endif ()
if (HAVE_LIB_NCURSES)
    set(GOODI_LIBS ${GOODI_LIBS} ncurses)
endif ()

CHECK_LIBRARY_EXISTS(sotcptap init_server "" HAVE_LIB_TCPTAP_SHARED)
if (HAVE_LIB_TCPTAP_SHARED)
    set(LIBS_SO ${LIBS_SO} sotcptap)
else ()
    message (WARNING
        "TCP-TAP (so) not found. Will not build what's dependent of TCP-TAP (so)")
endif ()

FIND_LIBRARY(HAVE_LIB_TCPTAP_STATIC atcptap)
FIND_LIBRARY(HAVE_LIB_LIBLOG_STATIC liblog_static)
if (HAVE_LIB_TCPTAP_STATIC AND HAVE_LIB_LIBLOG_STATIC)
    set(LIBS_A ${LIBS_A} atcptap)
else ()
    message (WARNING
        "TCP-TAP (a) not found. Will not build what's dependent of TCP-TAP (a)")
endif ()

# Adapt library path where supported or different
message (STATUS "Checking for (p)Threads")
find_package(Threads)

# Functions that might be missing and need worka-arounding
# Both of these are needed for either of the test to work (strange...)
set(CMAKE_REQUIRED_INCLUDES pthread.h)
set(CMAKE_REQUIRED_LIBRARIES pthread)

CHECK_FUNCTION_EXISTS("pthread_cancel"  HAVE_PTHREAD_CANCEL_F)
CHECK_SYMBOL_EXISTS("pthread_cancel" "pthread.h" HAVE_PTHREAD_CANCEL_S)

CHECK_FUNCTION_EXISTS("pthread_create"  HAVE_PTHREAD_CREATE_F)
CHECK_SYMBOL_EXISTS("pthread_create" "pthread.h" HAVE_PTHREAD_CREATE_S)

#-------------------------------------------------------------------------------
# X buildtool options (gcc)
#-------------------------------------------------------------------------------
set(SYSROOT
    ${CMAKE_SYSROOT}
    CACHE STRING
    "System path (--system=)")

# Check built-in preprocessor for tool-chain by run a test-compile but
# without a header file.
# Method is independent of CMake version and cmake x-dev tools plugin.
# Note that this method is not fool-proof if x-tools change after first
# cmake configuration Method is independent of CMake version and cmake x-dev
# tools plugin.
CHECK_SYMBOL_EXISTS(__ANDROID__ "" HAVE_CC_TARGET_CAN_ANDROID)

if (HAVE_CC_TARGET_CAN_ANDROID)
    message( "** Compiler can build for Android" )
    set(XB_LUA_DFLT_AF_UNIX_DIR "/data/local/tmp")
else ()
    set(XB_LUA_DFLT_AF_UNIX_DIR "/tmp")
endif ()

set(XB_LUA_AF_UNIX_DIR
    ${XB_LUA_DFLT_AF_UNIX_DIR}
    CACHE STRING
    "Domain-sockets directory")

#-------------------------------------------------------------------------------
# Build-tool options
#-------------------------------------------------------------------------------
set(CMAKE_EXTRA_C_FLAGS
    "${CMAKE_EXTRA_C_FLAGS}"
    CACHE STRING
    "Compiler options appended to CMAKE_C_FLAGS")

option(XB_LUA_EXPORT_COMPILE_COMMANDS
    "Generate compile_commands.json" ON)
mark_as_advanced(XB_LUA_EXPORT_COMPILE_COMMANDS)

set (CMAKE_EXPORT_COMPILE_COMMANDS
    ${XB_LUA_EXPORT_COMPILE_COMMANDS}
    CACHE BOOL
    "Overloaded cmake global cached CMAKE_EXPORT_COMPILE_COMMANDS"
    FORCE
)

if (NOT GOODI_LIBS STREQUAL "")
    # Offer option if there is a chance it would build
    set(MSG "Build with linux goodies. I.e. readline support")
    set(MSG "${MSG} including dependencies: dl, ncurses, history")
    option(ENABLE_LINUX_GOODIES ${MSG} ON)
else ()
    set(ENABLE_LINUX_GOODIES OFF)
endif ()

list(APPEND CMAKE_EXTRA_C_FLAGS "-std=gnu11 -DLUA_COMPAT_5_2")
if (ENABLE_LINUX_GOODIES)
    list(APPEND CMAKE_EXTRA_C_FLAGS "-DLUA_USE_READLINE")
else ()
    unset(GOODI_LIBS)
endif ()

#-------------------------------------------------------------------------------
# X build-tool options (this project's x-tool handling)
#-------------------------------------------------------------------------------
if (NOT SYSROOT STREQUAL "")
    message( "** INFO: SYSROOT was either set or defaulted from toolchain file" )
    set(CMAKE_SYSROOT "${SYSROOT}")
    list(APPEND CMAKE_EXTRA_C_FLAGS "--sysroot=${CMAKE_SYSROOT}")
endif ()

list(JOIN CMAKE_EXTRA_C_FLAGS " " CMAKE_EXTRA_C_FLAGS)
set(CMAKE_C_FLAGS
    "${CMAKE_EXTRA_C_FLAGS} \
    -no-integrated-cpp -Wno-unused-function -O2 -Wall")
set(CMAKE_C_FLAGS_DEBUG
    "${CMAKE_EXTRA_C_FLAGS} \
    -no-integrated-cpp -Wno-unused-function -Wno-unused -g3 -ggdb3 -O0 -Wall")
set(CMAKE_C_FLAGS_RELEASE
    "${CMAKE_EXTRA_C_FLAGS} \
    -no-integrated-cpp -Wno-unused-function -Os -Wall")

message(STATUS "CMAKE_C_FLAGS: ${CMAKE_C_FLAGS}")
message(STATUS "CMAKE_C_FLAGS_DEBUG: ${CMAKE_C_FLAGS}")
message(STATUS "CMAKE_C_FLAGS_RELEASE: ${CMAKE_C_FLAGS}")

configure_file (
    "${PROJECT_SOURCE_DIR}/xb_lua_config.h.in"
    "${PROJECT_BINARY_DIR}/xb_lua_config.h"
)

# add the binary tree to the search path for include files
# so that we will find Config.h
include_directories("${PROJECT_BINARY_DIR}")

### core library #############################
set(XB_LUA_CORE_O
    lua/lapi.c
    lua/lcode.c
    lua/lctype.c
    lua/ldebug.c
    lua/ldo.c
    lua/ldump.c
    lua/lfunc.c
    lua/lgc.c
    lua/llex.c
    lua/lmem.c
    lua/lobject.c
    lua/lopcodes.c
    lua/lparser.c
    lua/lstate.c
    lua/lstring.c
    lua/ltable.c
    lua/ltm.c
    lua/lundump.c
    lua/lvm.c
    lua/lzio.c
    lua/ltests.c
)
add_library(xb_luacore SHARED ${XB_LUA_CORE_O})
target_link_libraries (xb_luacore ${EXTRA_LIBS})

add_library(xb_luacore_static STATIC ${XB_LUA_CORE_O})
target_link_libraries (xb_luacore_static ${EXTRA_LIBS})

set_target_properties(xb_luacore PROPERTIES PUBLIC_HEADER
    "include-lua/xb-lua/lua.h")
set_property(TARGET xb_luacore APPEND PROPERTY PUBLIC_HEADER
    "${CMAKE_CURRENT_BINARY_DIR}/xb_lua_config.h")
set_property(TARGET xb_luacore APPEND PROPERTY PUBLIC_HEADER
    "include-lua/xb-lua/lprefix.h")
set_property(TARGET xb_luacore APPEND PROPERTY PUBLIC_HEADER
    "include-lua/xb-lua/luaconf.h")

### aux library #############################
set(XB_LUA_AUX_O
    lua/lauxlib.c
)
add_library(xb_luaaux SHARED ${XB_LUA_AUX_O})
target_link_libraries (xb_luaaux ${EXTRA_LIBS})

add_library(xb_luaaux_static STATIC ${XB_LUA_AUX_O})
target_link_libraries (xb_luaaux_static ${EXTRA_LIBS})

set_target_properties(xb_luaaux PROPERTIES PUBLIC_HEADER
  "include-lua/xb-lua/lauxlib.h")

### "lualib" library #############################
set(XB_LUA_LIB_O
    lua/lbaselib.c
    lua/ldblib.c
    lua/liolib.c
    lua/lmathlib.c
    lua/loslib.c
    lua/ltablib.c
    lua/lstrlib.c
    lua/lutf8lib.c
    lua/lbitlib.c
    lua/loadlib.c
    lua/lcorolib.c
    lua/linit.c
)
add_library(xb_lualib SHARED ${XB_LUA_LIB_O})
target_link_libraries (xb_lualib ${EXTRA_LIBS})

add_library(xb_lualib_static STATIC ${XB_LUA_LIB_O})
target_link_libraries (xb_lualib_static ${EXTRA_LIBS})

set_target_properties(xb_lualib PROPERTIES PUBLIC_HEADER
  "include-lua/xb-lua/lualib.h")

### Lua executable #############################
set(XB_LUA_SOURCE
    lua/lua.c
)
option(BUILD_XB_LUA "Build xb_lua (main interpreter)" ON)
if (BUILD_XB_LUA)
    add_executable(xb_lua ${XB_LUA_SOURCE})
    target_link_libraries (xb_lua
        xb_luacore
        xb_luaaux
        xb_lualib
        ${EXTRA_LIBS}
        ${GOODI_LIBS}
    )
    list(APPEND XB_LUA_TARGETS xb_lua)
endif ()

### lua-console & terminal lib #################
set(XB_LUA_CONSOLE
    lua_console_533.c
    luabind.c
    service_terminal.c
    argutil.c
)

add_library(xb_luaconsole SHARED ${XB_LUA_CONSOLE})
target_link_libraries (xb_luaconsole
    xb_luacore
    xb_luaaux
    xb_lualib
    ${EXTRA_LIBS}
    ${LIBS_SO}
)

add_library(xb_luaconsole_static STATIC ${XB_LUA_CONSOLE})
target_link_libraries (xb_luaconsole_static
    xb_luacore_static
    xb_luaaux_static
    xb_lualib_static
    ${EXTRA_LIBS}
    ${GOODI_LIBS}
    ${LIBS_A}
)

set_target_properties(xb_luaconsole PROPERTIES PUBLIC_HEADER
    "include/xb-lua/console.h")
set_property(TARGET xb_luaconsole APPEND PROPERTY PUBLIC_HEADER
    "include/xb-lua/argutil.h")
set_property(TARGET xb_luaconsole APPEND PROPERTY PUBLIC_HEADER
    "include/xb-lua/luabind.h")

### Lua examples ########################
set(XB_LUA_TERMINAL_SERVICE_DEMO
    examples/terminal_service.c
    examples/lapi_demolib.c
)

if (HAVE_LIB_TCPTAP_SHARED)
    set(XB_CON_DEF ON)
else ()
    set(XB_CON_DEF OFF)
endif ()
option(BUILD_SHARED_XB_LUA_EXAMPLES
    "Build shared test/example-program(s)" ${XB_CON_DEF})

if (BUILD_SHARED_XB_LUA_EXAMPLES)
    add_executable(xb_console ${XB_LUA_TERMINAL_SERVICE_DEMO})
    target_link_libraries (xb_console
        xb_luaconsole
        ${GOODI_LIBS}
    )
    list(APPEND XB_LUA_TARGETS xb_console)
endif ()

if (HAVE_LIB_TCPTAP_STATIC)
    set(XB_CON_DEF ON)
else ()
    set(XB_CON_DEF OFF)
endif ()

option(BUILD_STATIC_XB_LUA_EXAMPLES
    "Build static test/example-program(s)" ${XB_CON_DEF})

if (BUILD_STATIC_XB_LUA_EXAMPLES)
    add_executable(xb_console-static ${XB_LUA_TERMINAL_SERVICE_DEMO})
    target_link_libraries (xb_console-static
        xb_luaconsole_static
        xb_luacore_static
        xb_luaaux_static
        xb_lualib_static
        atcptap
        liblog_static
        readline
    )
    list(APPEND XB_LUA_TARGETS xb_console-static)
endif ()

# Library targets to install (unconditionally all)
list(APPEND XB_LUA_TARGETS
    xb_luacore
    xb_luaaux
    xb_lualib
    xb_luaconsole
    xb_luacore_static
    xb_luaaux_static
    xb_lualib_static
    xb_luaconsole_static
)

# Install:
install(TARGETS       ${XB_LUA_TARGETS}
        RUNTIME       DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
        LIBRARY       DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        ARCHIVE       DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${PROJECT_NAME})

# Install package generation
# --------------------------
set(CPACK_PACKAGE_CONTACT "Michael Ambrus")
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
set(CPACK_PACKAGE_DESCRIPTION_FILE
    ${PROJECT_SOURCE_DIR}/DESCRIPTION)
set(CPACK_RESOURCE_FILE_LICENSE
    ${PROJECT_SOURCE_DIR}/LICENSE)

# Append or change if/when needed
list(APPEND PROJ_DEPENDS "liblog    (>= 0.3.12)")
list(APPEND PROJ_DEPENDS "tcp-tap   (>= 0.8.5)")
# Buit with, therefor required
if (ENABLE_LINUX_GOODIES)
    list(APPEND PROJ_DEPENDS "libreadline8 (>= 8.0)")
endif ()

list(JOIN PROJ_DEPENDS ", " PROJ_DEPENDS_STR)
message(STATUS "Dependencies: ${PROJ_DEPENDS_STR}")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "${PROJ_DEPENDS_STR}")

set(SPECIFIED_CPACK
    ${SPECIFIED_CPACK}DEB
    CACHE STRING
    "Specify cpack generator (press enter to toggle choices)")

set_property(CACHE SPECIFIED_CPACK
    PROPERTY STRINGS
    "NSIS ZIP" "DEB" "TGZ" "RPM")

if (NOT SPECIFIED_CPACK STREQUAL "")
    set(CPACK_GENERATOR ${SPECIFIED_CPACK})
endif ()

include(CPack)

