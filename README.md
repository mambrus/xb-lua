# xb-lua

![](http://www.ambrus.se/~michael/passets/xblua_logo_readme.png)

This project wraps a specific `lua` (v5.3.3) into an embedded lua-VM of any
C-application, providing it a shell console on a multi-session TCP-server port.

Project provides X-build support.

Read more on the
[wiki](https://gitlab.com/mambrus/xb-lua/-/wikis/home)

## Release history

(None yet)
