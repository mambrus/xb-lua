/*******************************************************************************
 *  Helpers for handling argv, argc
 *  Copyright: Michael Ambrus, 2016
 ******************************************************************************/
#ifndef argutil_h
#define argutil_h

int args2argv(char ***new_argv, char *cmd, char *args);

#endif                          // argutil_h
